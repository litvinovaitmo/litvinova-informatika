<?php
require_once 'connection.php'; 
 
$link = mysqli_connect($host, $user, $password, $database) 
    or die ("Ошибка подключения" . mysqli_error($link));

echo "Успешное подключение";
 
$sql = "CREATE TABLE airline (
  company varchar(50) NOT NULL,
  license int(8) NOT NULL,
  ceo varchar(50) NOT NULL,
  PRIMARY KEY (`company`)
) ";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql = "INSERT INTO airline values ('AirBritan', 789, 'zxc'),
    ('AirChina', 456, 'vbn'),
    ('AirRussia', 123, 'asd')";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql ="CREATE TABLE employee (
  employee_id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  position varchar(50) NOT NULL,
  education varchar(40) NOT NULL,
  experience int(2) NOT NULL,
  PRIMARY KEY (employee_id)
) ";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql = "INSERT INTO employee values ( 123, 'Ivanov A.I.', 'scipper', 'Airschool1', 2 ),
    ( 134, 'Doronin G.O.', 'scipper', 'Airschool2', 8 ),
    ( 145, 'Voloshina A.P.', 'stewardess', 'Airschool2', 1 ),
    ( 158, 'Artamonova O.S.', 'stewardess', 'Airschool1', 3 ),
    ( 189, 'Lorionov A.S.', 'stewardess', 'Airschool1', 5 )";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql ="CREATE TABLE machine (
  model varchar(10) NOT NULL,
  brand varchar(20) NOT NULL,
  seats int(4) NOT NULL,
  speed int(4) NOT NULL,
  PRIMARY KEY (model)
) ";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}


$sql = "INSERT INTO machine values ( 'H4589', 'Airbus', 180, 350),
    ( 'I4579', 'Boing', 460, 400),
    ( 'J2589', 'Airbus', 400, 350),
    ( 'L2147', 'Airbus', 350, 350),
    ( 'O2458', 'Boing', 200, 400)";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}
$sql ="CREATE TABLE route (
  number varchar(10) NOT NULL,
  departure varchar(50) NOT NULL,
  arrival varchar(50) NOT NULL,
  distance int(5) NOT NULL,
  PRIMARY KEY (number)
) ";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql = "INSERT INTO route values ( 'L2145', 'NSK', 'MSK', 2500),
    ( 'L5695', 'SPB', 'UKK', 3200),
    ( 'L8399', 'POD', 'RMK', 1700),
    ( 'L8965', 'UYG', 'DME', 2000)";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql ="CREATE TABLE service (
  name varchar(20) NOT NULL,
  address varchar(50) NOT NULL,
  license int(8) NOT NULL,
  PRIMARY KEY (name)
) ";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql = "INSERT INTO service values ('Service1', 'Spb', 1234),
    ('Service2', 'Msk', 4567),
    ('Service3', 'Nsk', 7890)";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql ="CREATE TABLE transit_landing (
  landing_code int(10) NOT NULL AUTO_INCREMENT,
  location varchar(50) NOT NULL,
  period int(3) NOT NULL,
  PRIMARY KEY (`landing_code`)
) ";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql = "INSERT INTO transit_landing values ( 147, 'L_SPB', 1),
    ('258', 'L_MSK', 2),
    ('369', 'L_NSK', 3)";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql ="CREATE TABLE plane (
  number varchar(5) NOT NULL,
  airline varchar(50) NOT NULL,
  service varchar(20) NOT NULL,
  PRIMARY KEY (number),
  KEY airline (airline),
  KEY service (service),
  FOREIGN KEY (airline) REFERENCES airline (company),
  FOREIGN KEY (service) REFERENCES service (name)
) ";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql = "INSERT INTO plane values ('A753', 'AirBritan', 'Service1'),
    ('B753', 'AirRussia', 'Service2'),
    ('C753', 'AirChina', 'Service3'),
    ('D753', 'AirBritan', 'Service1'),
    ('E753', 'AirRussia', 'Service3')";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql = "CREATE TABLE flight (
  number varchar(5) NOT NULL,
  departure_time datetime NOT NULL,
  arrival_time datetime NOT NULL,
  plane varchar(5) NOT NULL,
  PRIMARY KEY (number),
  KEY plane (plane),
  FOREIGN KEY (plane) REFERENCES plane (number)
) ";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}

$sql = "INSERT INTO flight values ('X123', '2018-04-25T11:30:00' , '2018-04-25T12:40:00', 'A753'),
    ('X124', '2018-04-25T15:00:00', '2018-04-25T20:50:00' , 'B753'),
    ('X125', '2018-04-25T20:30:00', '2018-04-26T09:25:00' , 'C753'),
    ('X126', '2018-04-25T23:55:00', '2018-04-26T06:45:00', 'D753')";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}


$sql = "CREATE TABLE crew (
  code int(10) NOT NULL AUTO_INCREMENT,
  flight varchar(5) NOT NULL,
  employee int(10) DEFAULT NULL,
  PRIMARY KEY (code),
  KEY flight (flight),
  KEY employee (employee),
  FOREIGN KEY (flight) REFERENCES flight (number),
  FOREIGN KEY (employee) REFERENCES employee (employee_id)
)";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}


$sql = "INSERT INTO crew values (140, 'X123', 123),
    (141, 'X124', 134),
    (142, 'X125', 145),
    (143, 'X126', 158) ";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}


$sql= "CREATE TABLE landing (
  code int(10) NOT NULL AUTO_INCREMENT,
  flight varchar(5) NOT NULL,
  landing_code int(10) NOT NULL,
  PRIMARY KEY (code),
  KEY flight (flight),
  KEY landing_code (landing_code),
  FOREIGN KEY (flight) REFERENCES flight (number),
  FOREIGN KEY (landing_code) REFERENCES transit_landing (landing_code)
) ";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}


$sql = "INSERT INTO landing values (701, 'X123', 147),
    (702, 'X124', 258),
    (703, 'X125', 369)";
if (mysqli_query($link, $sql)) {
    echo "Успешно";
} else {
    echo "Ошибка" . mysqli_error($link);
}


mysqli_close($link);
?>
